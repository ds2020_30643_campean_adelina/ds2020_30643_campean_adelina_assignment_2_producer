package com.assignment2.rabbitmq;


import java.io.Serializable;
import java.util.UUID;


public class Message implements Serializable {
    private UUID message_id;
    private UUID patient_id;
    private String start_time;
    private String end_time;
    private String activity_label;

    public Message() {
    }

    public Message(UUID message_id, UUID patient_id, String start_time, String end_time, String activity_label) {
        this.message_id = message_id;
        this.patient_id = patient_id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity_label = activity_label;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message_id=" + message_id +
                ", patient_id=" + patient_id +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", activity_label='" + activity_label + '\'' +
                '}';
    }

    public UUID getMessage_id() {
        return message_id;
    }

    public void setMessage_id(UUID message_id) {
        this.message_id = message_id;
    }

    public UUID getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getActivity_label() {
        return activity_label;
    }

    public void setActivity_label(String activity_label) {
        this.activity_label = activity_label;
    }
}
