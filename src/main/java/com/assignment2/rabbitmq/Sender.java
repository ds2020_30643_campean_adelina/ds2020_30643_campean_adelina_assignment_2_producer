package com.assignment2.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;


@Service
public class Sender {
    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    private final RabbitTemplate rabbitTemplate;
    private ApplicationContext applicationContext;
    private String beanName;
    private int noLines=0;
    private int noMessages=0;
    private ArrayList<Message> messages = new ArrayList<>();

    public Sender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        messages = readFromFile();
    }

    public ArrayList<Message> readFromFile() {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            File myObj = new File("activity.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();

                Message m1 = new Message();

                m1.setMessage_id(UUID.randomUUID());
                m1.setPatient_id(UUID.fromString("f060d306-cc95-4a2a-ab01-d352a989021c"));

                String[] parts = data.split("\t\t");
                String start = parts[0];
                String end = parts[1];
                String activity = parts[2];


                if(activity.contains("\t")) {
                   activity = activity.replace("\t","");
                   // activity = activity.substring(0, activity.length() - 1);
                }

                m1.setStart_time(start);
                m1.setEnd_time(end);
                m1.setActivity_label(activity);

                noLines+=1;
                messages.add(m1);
                //System.out.println(data);

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return messages;
    }

    @Scheduled(fixedDelay = 1000L)
    public void sendMessage() {

        if(noLines==noMessages) {
            return;
        }

         noMessages+=1;
         rabbitTemplate.convertAndSend(RabbitmqApplication.EXCHANGE, RabbitmqApplication.ROUTING_KEY, messages.get(noMessages-1).toString());
         log.info("Message sent successfully!");

    }

    private void stopScheduledTask()
    {
        ScheduledAnnotationBeanPostProcessor bean = applicationContext.getBean(ScheduledAnnotationBeanPostProcessor.class);
        bean.postProcessBeforeDestruction(this, beanName);
    }


    public void setApplicationContext(ApplicationContext applicationContext)
    {
        this.applicationContext = applicationContext;
    }


    public void setBeanName(String beanName)
    {
        this.beanName = beanName;
    }



}
